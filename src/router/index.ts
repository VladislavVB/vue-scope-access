import { createRouter, createWebHistory } from "vue-router";
import { loadLayoutMiddleware } from "@/router/middleware/loadLayout.middleware";
import { RouteNamesEnum } from "@/router/router.types";
import { AppLayoutsEnum } from "@/layouts/layouts.types";
import { fetchAuthDataMiddleware } from "@/router/middleware/fetchAuthData.middleware";
import { accessGuardMiddleware } from "@/router/middleware/accessGuard.middleware";
import { AccessScopesEnum } from "@/modules/auth/auth.types";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: RouteNamesEnum.home,
      component: () => import("@/modules/common/pages/HomePage.vue"),
      meta: {
        layout: AppLayoutsEnum.default,
        accessScopes: [AccessScopesEnum.LOGGED_IN],
      },
    },
    {
      path: "/books",
      name: RouteNamesEnum.books,
      component: () => import("@/modules/books/pages/BooksPage.vue"),
      meta: {
        layout: AppLayoutsEnum.default,
        accessScopes: [AccessScopesEnum.CAN_READ_BOOKS],
      },
    },
    {
      path: "/cars",
      name: RouteNamesEnum.cars,
      component: () => import("@/modules/cars/pages/CarsPage.vue"),
      meta: {
        layout: AppLayoutsEnum.default,
        accessScopes: [AccessScopesEnum.CAN_READ_CARS],
      },
    },
    {
      path: "/access-error",
      name: RouteNamesEnum.accessError,
      component: () => import("@/modules/auth/pages/AccessErrorPage.vue"),
      meta: {
        layout: AppLayoutsEnum.error,
      },
    },
  ],
});

router.beforeEach(fetchAuthDataMiddleware);
router.beforeEach(accessGuardMiddleware);
router.beforeEach(loadLayoutMiddleware);

export default router;
