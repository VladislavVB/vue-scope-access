import { useAuthService } from "@/modules/auth/auth.service";

export function fetchAuthDataMiddleware(): Promise<void> {
  const { fetchUserOnes } = useAuthService();
  return fetchUserOnes();
}
